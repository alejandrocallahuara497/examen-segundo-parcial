//NOMBRE: JOSE ALEJANDRO COLQUE CALLAHUARA
//RU:76542
//C.I:8598092
package com.mycompany.resprimerej1;
import java.util.Scanner;
import javaapplication1.operations;
import javaapplication1.pila;
public class main {
    public static void main (String[] args){
        Scanner sc = new Scanner(System. in);
        pila p_use;
        operations o_use;
        int  sl,sz,op;
        int yes = 0;
        System.out.println("Que tipo de desea crear?");
        System.out.println("1. Dinámica");
        System.out.println("2. limite");
        System.out.println("3. Ambos");
        sl = sc.nextInt();
        System.out.println("Tamanio de la pila");
        sz = sc.nextInt();
        p_use = typePila(sl,sz);
        o_use = operationsPila(sl,p_use,sz);
        while(yes == 0 && p_use != null){
            System.out.println("1. Aniadir elementos");    
            System.out.println("2. Eliminar elementos");
            System.out.println("3. Cuantos elementos exiten y espacios libres");
            System.out.println("4. maximo y minimo");
            System.out.println("5. Buscar elementos");
            System.out.println("6. Ordenar elementos");
            System.out.println("7. Ver todos");
            System.out.println("n. salir");
            System.out.println("Que desea realizar");
            op = sc.nextInt();
            switch(op){
                case 1:
                    System.out.println("Que valores desea ingresar");        
                    int count = 1;
                    while(count <= sz && !p_use.full()){
                         String vs = sc.next();
                         if(sl == 1){
                             String vs_string = vs;
                             o_use.insert(vs_string);
                         }else if(sl == 2){
                             int vs_integer = Integer.parseInt(vs);
                             o_use.insert(vs_integer);
                         }else if(sl == 3){
                             boolean numeric = true;
                             numeric = vs.matches("-?\\d+(\\.\\d+)?");
                             if(numeric){
                                 int vbi_object = Integer.parseInt(vs);
                                 o_use.insert(vbi_object);
                             }else{
                                 String vbs_object = vs;
                                 o_use.insert(vbs_object);
                             }
                         }
                    }
                break;
                case 2:
                    o_use.remove();
                break;
                case 3:
                    o_use.occupiedFree();
                break;
                case 4:
                    o_use.maxMin();
                break;
                case 5:
                    System.out.println("Que desea buscar");
                    String s = sc.next();
                    o_use.searchStack(s);
                break;
                case 6:
                    System.out.println("ordenando los datos");
                    o_use.sortStack();// 1 5 2 4 3
                break;
                case 7:
                    System.out.println("todos los datos");
                    o_use.showall();
                break;
            }
            System.out.println("Desea continuar? precione 0 para si o cualquier otro para finalizar");
            yes = sc.nextInt();
        }
        
    }
    
    public static pila typePila(int type, int size){
        pila p;
        switch(type){
            case 1:
                pila<String> p_string = new pila<>(size);
                p = p_string;
                System.out.println("pila string");
            break;
            case 2:
                pila<Integer> p_integer= new pila<>(size);
                p = p_integer;
                System.out.println("pila enteros");
            break;
            default:
                pila<Object> p_object = new pila<>(size);
                p = p_object;
                System.out.println("pila cualquier tipo");
            break;
        }
        return p;
    }
    
    public static operations operationsPila(int type,pila p,int size){
        operations o;
        switch(type){
            case 1:
                operations<String> o_string = new operations<>(p,size);
                o = o_string;
            break;
            case 2:
                operations<Integer> o_integer = new operations<>(p,size);
                o = o_integer;                
            break;
            default:
                operations<Object> o_object = new operations<>(p,size);
                o = o_object;
            break;
        }
        return o;
    }
}


