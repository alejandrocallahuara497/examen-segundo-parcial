//NOMBRE: JOSE ALEJANDRO COLQUE CALLAHUARA
//RU:76542
//C.I:8598092
package javaapplication1;
public class operations<T> {
    pila<T> po;
    int size;
    public operations(pila p, int size){
       this.po = p;
       this.size = size;
    }
    
    public void insert(T element){
        po.push(element);
    }
    public void remove(){
        System.out.println("elemento "+po.pop()+" fue eliminado");
    }
    
    public void occupiedFree(){
        System.out.println("Elementos existentes: "+po.size()+" - Espacios libres: "+ po.free());
    }
    
    public void maxMin(){
        pila<T> paux1 = new pila<>(size);
        int min = Integer.MIN_VALUE;
        int max = Integer.MAX_VALUE;
        String resultmin = null;
        String resultmax = null;
        while(!po.empty()){
            String temp = String.valueOf(po.peek());
            char character = temp.charAt(0);
            int ascii = (int) character;
            if(ascii <= max){
                resultmin = temp;
                max = ascii;
            }
            if(ascii >= min){
                resultmax = temp;
                min = ascii;
            }
            paux1.push(po.pop());
        }
        while(!paux1.empty()){
            po.push(paux1.pop());
        }
        System.out.println("Minimo: "+resultmin+" - Maximo: "+resultmax);
    }
    
    public void searchStack(String search){
        pila<T> paux1 = new pila<>(size);
        boolean exist = false;
        String temp = null;
        while(!po.empty()){
            temp = String.valueOf(po.peek());
            if(temp.equals(search)){
                exist = true;
            }
            paux1.push(po.pop());
        }
        while(!paux1.empty()){
            po.push(paux1.pop());
        }
        
        if(exist){
            System.out.println("elemento "+ search +" fue encontrado");
        }else{
            System.out.println("elemento "+ search +" no encontrado");
        }
    }
    
    public void sortStack(){
        pila<T> tp = new pila<>(size);
        pila<T> tpi = new pila<>(size);
        while(!po.empty()){
            tpi.push(po.pop());
            int currentData = (int) (String.valueOf(tpi.peek())).charAt(0);
            int tppeek = (int) (String.valueOf(tp.peek())).charAt(0);
            while(!tp.empty() && tppeek < currentData){
                po.push(tp.pop());
            }
            tp.push(tpi.pop());
        }
        
        po = tp;
    }
    
    public void showall(){
        while(!po.empty()){
            System.out.println(po.pop());
        }
    }
}
