//NOMBRE: JOSE ALEJANDRO COLQUE CALLAHUARA
//RU:76542
//C.I:8598092
package javaapplication1;
public class pila<T> {
    T[] arr; //un arreglo 
    int maxSize;
    int top;
    public pila(int n){
        this.maxSize = n;
        this.arr = (T[]) new Object[maxSize];
        this.top =0;
    }
    public boolean empty(){
        return top == 0;
    }
    
    public boolean full(){
        return top == maxSize;
    }
    
    public int size(){
        return top;
    }
    
    public int free(){
        return maxSize - top;
    }
    
    public void push(T str){
        if(top < maxSize){
            arr[top] = str;
            top++;
        }
    }
    
    public T peek(){
        if(top>0){
             return arr[top-1];
        }else{
            return null;
        }
    }
    
    public T pop(){
        T temp = null;
        if(top>0){
            temp = arr[top-1];
            arr[top-1] = null;
            top--;
        }
        return temp;
    }
}